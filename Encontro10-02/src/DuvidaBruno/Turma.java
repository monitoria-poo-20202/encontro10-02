package DuvidaBruno;
public class Turma {
	
	private String codigoDaSala;
	private String serie;
	private Aluno aluno;
	
	public Turma(String codigoDaSala, String serie, Aluno aluno) {
		super();
		this.codigoDaSala = codigoDaSala;
		this.serie = serie;
		this.aluno = aluno;
	}
	
	
//	public Turma(String codigoDaSala, String serie) {
//		super();
//		this.codigoDaSala = codigoDaSala;
//		this.serie = serie;
//	}


	public String getCodigoDaSala() {
		return codigoDaSala;
	}


	public void setCodigoDaSala(String codigoDaSala) {
		this.codigoDaSala = codigoDaSala;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public Aluno getAluno() {
		return aluno;
	}


	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	

}
