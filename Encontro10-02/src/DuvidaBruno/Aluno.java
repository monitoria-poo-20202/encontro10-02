package DuvidaBruno;

public class Aluno {
	
	private String nome;
	private String idade;
	
	public Aluno(String nome, String idade) {
		super();
		this.nome = nome;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public void printar() {
		System.out.println("Aluno [nome=" + nome + ", idade=" + idade + "]");
	}
	
}
