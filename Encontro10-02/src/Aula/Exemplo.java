package Aula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import DuvidaBruno.Aluno;

public class Exemplo {
	
	public static void main(String[] args) {
		
		/*
		List<String> listaString = new ArrayList<String>(Arrays.asList("um", "dois"));
		System.out.println(listaString.toString());
		
		List<Integer> listaInteger = new ArrayList<>(Arrays.asList(1, 2));
		listaInteger.add(3);
		System.out.println(listaInteger.toString());
		*/
				
		List<Aluno> listaAlunos;
		listaAlunos  = new ArrayList<>();
		/*
		listaAlunos.add(new Aluno("Fulano", "16"));
		listaAlunos.add(new Aluno("Fulano", "16"));
		//DuvidaBruno.Aluno@4eec7777
		//DuvidaBruno.Aluno@3b07d329
		
		*/
		
		Aluno a1 = new Aluno("Fulano", "16");
		Aluno a2 = new Aluno("Ciclano", "17");
		Aluno a3 = new Aluno("Beltrano", "18");
		
		List<Aluno> listaRedes  = new ArrayList<>(Arrays.asList(a1, a2, a3));
		listaAlunos.add(a3);
		System.out.println("Lista redes: " + listaRedes.size());
		System.out.println("Lista POO: " + listaAlunos.size());
		
//		listaAlunos.add(a1);
//		listaAlunos.remove(a1);
		
		System.out.println(listaRedes.contains(listaAlunos.get(0)));
		
		listaAlunos.addAll(listaRedes);
		
		System.out.println("Lista redes: " + listaRedes.size());
		System.out.println("Lista POO: " + listaAlunos.size());
		
		/* Foris */
		System.out.println("\nLista de Alunos em Redes:");
		for (int i = 0; i < listaRedes.size(); i++) {
//			listaRedes = [DuvidaBruno.Aluno@4aa298b7, DuvidaBruno.Aluno@7d4991ad, DuvidaBruno.Aluno@28d93b30]
//			get(i) -> pegar a posição -> listaRedes[1]...[n]
			listaRedes.get(i).printar();
		}
		Integer[] numeros = {6, 2, 8, 4};
		for (int i = 0; i < numeros.length; i++) {
			// Exemplo parecido com o tratado acima ( por baixo dos panos )
			System.out.print(numeros[i] + " ");
		}
		
		System.out.println();

		for (Aluno aluno : listaRedes) {
			aluno.printar();
		}
		
		
		for (Integer num : numeros) {
			System.out.print(num + " ");
		}
		
		System.out.println();
		
	}

}
